if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_LONGTERM_VARIANCE_SUMMARY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_LONGTERM_VARIANCE_SUMMARY]
GO
set quoted_identifier off
go
/*
Written By		: Krishna Reddy 
Created Date	: August 17 2018 
Description		: SF Case: 10038			Programmer: Krishna			Date: 08/17/2018
				  New report to show Variance details .
Modified Date	: 	
exec FMS_SP_LONGTERM_VARIANCE_SUMMARY 'north', '_All', 08,2018

*/

CREATE PROCEDURE dbo.FMS_SP_LONGTERM_VARIANCE_SUMMARY (@CompanyCode VARCHAR(15), @YardCode varchar(max)='_All', @Month int, @Year int)    
AS
BEGIN
		declare @stmt varchar(max)
	 
		
		SELECT YardCode, Branch,  Class, Salesperson, ActualExt, ListExt,
				Variance = (ActualExt -ListExt), VariancePerc = ((ActualExt-ListExt)/CASE WHEN ListExt = 0 THEN 1 ELSE ListExt END) * 100.0,
				AssistedBy,
				OnRentDate,
				BillingInformation,
				JobCity,
				CustomerCity
		FROM
		(SELECT FIM.YardCode, Branch = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FIM.YardCode), 
			Class = (select Description FROM FMS_UNIT_TYPES FUT WHERE FUT.UnitTypeID= (select UnitTypeID from FMS_UNITS FU where FU.UnitCode=FID.UnitCode)),
			Salesperson = (SELECT RTRIM(ISNULL(Lname,''))+ ', '+RTRIM(ISNULL(Fname,''))+' '+RTRIM(ISNULL(Mname,'')) FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode),
			ActualExt=isnull(FID.ActAmount,0),
			ListExt=isnull(FID.ActQty,0) * isnull(FBC.DefaultRate,0),
			BillingInformation = (select Description FROM FMS_BILLING_CODES FBC WHERE FBC.BillingCodeID =FID.BillingCodeID),
			AssistedBy = (select RTRIM(ISNULL(Fname,'')) + RTRIM(ISNULL(' ' + Mname,''))+RTRIM(ISNULL(' ' + Lname,''))  from FMS_ASSISTED_BY FAB where FAB.AssistedByCode = (select top 1 AssistedByCode FROM FMS_QUOTE_MASTER FQM WHERE FQM.QuoteNumb = (Select top 1 QuoteNumb FROM FMS_JOB_MASTER FJM where FJM.JobCode=FIM.JobCode))),
			OnRentDate = FID.DateStart,
			CustomerCity = FC.City,
			JobCity = FCL.City
			FROM FMS_INVOICE_DETAILS FID
			LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID,
			FMS_INVOICE_MASTER FIM
			LEFT OUTER JOIN FMS_CUSTOMERS FC ON FC.CustCode=FIM.CustCode
			LEFT OUTER JOIN FMS_CUSTOMER_LOCATIONS FCL ON FCL.CustLocationID=FIM.CustLocationID 
			WHERE FIM.InvoiceCode=FID.InvoiceCode
			and (FIM.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
			and (FIM.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)
			and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year
		) X
END
GO
 


