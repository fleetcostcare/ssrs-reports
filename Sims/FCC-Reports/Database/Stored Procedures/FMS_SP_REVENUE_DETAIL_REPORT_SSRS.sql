if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_REVENUE_DETAIL_REPORT_SSRS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_REVENUE_DETAIL_REPORT_SSRS]
GO


/*
Written By		: Krishna Reddy 
Created Date	: November 08 2006
Description		: SF Case# 9915		Programmer: Krishna 	Date: 08/20/2018
					New report for FCC SSRS.
				   SF Case# 10140		Programmer: Krishna 	Date: 09/11/2018
					Passing Username parameter and checking security for company and Yard with that User.
				  
Modified Date	: 	

exec FMS_SP_REVENUE_DETAIL_REPORT_SSRS 'NORTH', '1/1/2018', '04/10/2018', '_Allyards' 
*/

CREATE PROCEDURE DBO.FMS_SP_REVENUE_DETAIL_REPORT_SSRS (@Username varchar(20), @CompanyCode varchar(15),@Begdt DATETIME, @Enddt DATETIME,  @Yard varchar(max), @JobCode varchar(15))
	AS
	BEGIN

			DECLARE @GroupNumb char(2)--, @UserName varchar(20)
		--set @UserName = (select dbo.FMS_FN_LOGGED_USER())
			set @GroupNumb =  (select case when FUO.AllGroups = 1 then null else FU.GroupNumb end FROM FMS_USERS FU,  
					FMS_USER_OPTIONS FUO WHERE  FU.UserID = FUO.USERID and FUO.UserName = @UserName);
 		
		SELECT FIM.InvoiceCode, FIM.InvDate,  FIM.JobCode, FIM.PoNumb, 
		TotalBilled = (SELECT isnull(sum(FID.ActAmount),0)  FROM FMS_INVOICE_DETAILS FID with (nolock) WHERE FID.InvoiceCode = FIM.InvoiceCode),
		YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM with (nolock) WHERE FYM.YardCode=FJM.YardCode),
		CompanyName = (SELECT FCO.Name FROM FMS_COMPANIES FCO with (nolock) WHERE FIM.CompanyCode=FCO.CompanyCode), 
		CustomerName = (SELECT FC.Name1 FROM FMS_CUSTOMERS FC with (nolock) WHERE FC.CustCode = FIM.CustCode AND FIM.GroupNumb = FC.GroupNumb),
		FJM.CustCode, FJM.YardCode, FJM.GroupNumb, FJM.CompanyCode, FJM.JobSDate, FJM.JobEDate,
		DBCustName = (SELECT Custname from fms_db_identity)
		FROM fms_invoice_master FIM with (nolock), FMS_JOB_MASTER FJM with (nolock)  
		WHERE FJM.JobCode = FIM.JobCode  
		AND FIM.InvDate BETWEEN  @Begdt and @Enddt
		AND (FJM.CompanyCode = @CompanyCode Or isnull(FJM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @CompanyCode = '_All' then @Username else '' end,'C')) or @Username='_All') 
		and (FJM.YardCode in (select Value from dbo.fms_fn_StringSplit (@Yard, ',')) Or isnull(FJM.YardCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Yard = '_All' then @Username else '' end,'Y')) or @Username='_All') 
		and FIM.GroupNumb = isnull(@GroupNumb,FIM.GroupNumb) 
		and (FIM.JobCode = @JobCode or '_All'=@JobCode)
		ORDER BY FIM.InvoiceCode 
	END
	GO