USE [NexGenFMS]
GO

/****** Object:  StoredProcedure [dbo].[FMS_SP_UTILIZATION_BRANCHWISE]    Script Date: 8/15/2018 1:34:19 PM ******/
DROP PROCEDURE [dbo].[FMS_SP_UTILIZATION_BRANCHWISE]
GO

/****** Object:  StoredProcedure [dbo].[FMS_SP_UTILIZATION_BRANCHWISE]    Script Date: 8/15/2018 1:34:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




/*
Written By		: Krishna Reddy 
Created Date	:  
Description		:  
Modified Date	: 	
AG - 4/8 - Adjusted the filters, removed the function refererence for company/yards assoicated with windows id (to be added later when we integrate SSRS reports with NexGen)
exec FMS_SP_UTILIZATION_BRANCHWISE  '_All', '_All', 1, 2018, '_All','-1'

AG - 7/14 - Modified the date range logic to use Invoice Date, instead of Invoice Detail/From Date.
	- "ActAmt" is used instead of "AllocExpAmt"
SF Case 11248                Programmer: Krishna         Date: 07/01/2018
Added Condition for getting DE/Mobilization hours
*/

CREATE PROCEDURE [dbo].[FMS_SP_UTILIZATION_BRANCHWISE] (@CompanyCode VARCHAR(15), @YardCode varchar(2000)='_All', @Month int, @Year int, @UnitSchedType VARCHAR(MAX), @UnitTypeID integer)    
AS
BEGIN
		declare @stmt varchar(max), @LastMonth int, @LastMonthYear int
	 
			--set @YardCode =  replace (@YardCode, '''','''''')  -- convert any single quote to double
			--set @YardCode =   '' + replace (@YardCode, ',',''',''') + ''  -- to convert "," to "','"
			
			--set @UnitSchedType =  replace (@UnitSchedType, '''','''''')  -- convert any single quote to double
			--set @UnitSchedType =   '' + replace (@UnitSchedType, ',',''',''') + ''  -- to convert "," to "','"
		 
		  set @LastMonth = case when @Month = 1 then 12 else @Month-1 end
		  set @LastMonthYear = case when @Month = 1 then @Year-1 else @Year End

		SELECT Company = Name, CompanyCode,
		YardCode,
		YardName,
		UnitSchedType, Schedule,  UnitTypeID, UnitType = Description,
		UnitCode,
		UnitName,
		CASE WHEN LEFT(UnitCode,1) like '[a-z]%' THEN 'Crane' else 'Lift' end as UnitGroup,
		DatePurchased, 
		VehicleMake, VehicleModel, SerialNumber,
		CurrentMonth_Dollars=round(CurrentMonth_Dollars,0), CurrentMonth_Hours, 
		Current_Util = round((CurrentMonth_Hours / TargetMonthHrs) * 100,1),
		LastMonth_Dollars=round(LastMonth_Dollars,0), LastMonth_Hours, 
		LastMonth_Util = round((LastMonth_Hours / TargetMonthHrs) * 100,1)
		FROM
		(SELECT	FCO.Name, 
				FU.CompanyCode,  
				FU.UnitSchedType, 
				Schedule = FST.Description,
				FU.UnitTypeID, FUT.Description, 
				FU.UnitCode, FU.DatePurchased,  
				FU.UnitName,
				FU.VehicleMake, FU.VehicleModel,
				FU.SerialNumber, 
				FU.YardCode, YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FU.YardCode), 

				CurrentMonth_Hours = (SELECT ISNULL(sum(case when MOB.Hours is null then  FID.AllocHrs  else (FID.ActQty * MOB.Hours) end),0) 
										FROM FMS_INVOICE_MASTER FIM with (nolock), FMS_INVOICE_DETAILS FID with (nolock) 
										LEFT OUTER JOIN SimsReporting..Mobilization_Hours MOB ON FID.BillingCodeId=MOB.BillingCodeID 
																								and MOB.UnitSchedType=FID.UnitSchedType 
																								and MOB.UnitTypeID=FID.UnitTypeID
																								and MOB.MeasureCode in ('EA', 'LS')   
										WHERE FID.UnitCode = FU.UnitCode and FIM.InvoiceCode=FID.InvoiceCode and month(FIM.InvDate) = @Month 
										and  Year(FIM.InvDate) = @Year), 

				CurrentMonth_Dollars  = (SELECT ISNULL(SUM(FID.ActAmount),0) FROM FMS_INVOICE_MASTER FIM with (nolock)
										INNER JOIN FMS_INVOICE_DETAILS FID with (nolock) ON FIM.InvoiceCode =FID.InvoiceCode  
										WHERE FID.UnitCode = FU.UnitCode and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year), 
				
				LastMonth_Hours = (SELECT ISNULL(sum(case when MOB.Hours is null then  FID.AllocHrs  else (FID.ActQty * MOB.Hours) end),0) 
										FROM FMS_INVOICE_MASTER FIM with (nolock), FMS_INVOICE_DETAILS FID with (nolock) 
										LEFT OUTER JOIN SimsReporting..Mobilization_Hours MOB ON FID.BillingCodeId=MOB.BillingCodeID 
																								and MOB.UnitSchedType=FID.UnitSchedType 
																								and MOB.UnitTypeID=FID.UnitTypeID
																								and MOB.MeasureCode in ('EA', 'LS')  
										WHERE FID.UnitCode = FU.UnitCode and FIM.InvoiceCode=FID.InvoiceCode 
										and month(FIM.InvDate) = @LastMonth and  Year(FIM.InvDate) = @LastMonthYear),   
										

				LastMonth_Dollars  = (SELECT ISNULL(SUM(FID.ActAmount),0) FROM FMS_INVOICE_MASTER FIM with (nolock)
										INNER JOIN FMS_INVOICE_DETAILS FID with (nolock) ON FIM.InvoiceCode =FID.InvoiceCode  
										WHERE FID.UnitCode = FU.UnitCode and month(FIM.InvDate) = @LastMonth and  Year(FIM.InvDate) = @LastMonthYear), 
										
			 TargetMonthHrs = case when isnull(FUT.TargetMonthHrs,0) = 0 then 1 else FUT.TargetMonthHrs  end
		FROM FMS_UNITS FU with (nolock) 
		LEFT OUTER JOIN FMS_UNIT_TYPES FUT with (nolock) ON FUT.UnitTypeID = FU.UnitTypeID 
		LEFT OUTER JOIN FMS_COMPANIES FCO with (nolock) ON FCO.CompanyCode = FU.CompanyCode 
		LEFT OUTER JOIN FMS_SCHEDULE_TYPES FST with (nolock) ON FST.UnitSchedCode=FU.UnitSchedType
		where FU.UnitTypeID IN (SELECT UnitTypeID FROM FMS_UNIT_TYPES WHERE TrackUnit = 1) 
		--and (FU.CompanyCode = @CompanyCode  OR isnull(FU.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_AllCompany' in (@CompanyCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'C')))
		--and (FU.YardCode in (@YardCode) OR isnull(FU.YardCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_AllYards' in (@YardCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'Y')))
		and (FU.CompanyCode = @CompanyCode  OR @CompanyCode='_All')
		and (FU.YardCode in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@YardCode,',')) OR @YardCode='_All')
		and FU.Status = 1 
		and (FST.Description like '%Crane%' or FST.Description like '%Lift%')
		and (FU.UnitTypeID in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitTypeID,',')) OR ('-1' in (@UnitTypeID)))
		and (FU.UnitSchedType in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')) OR ('_All' in (@UnitSchedType)))
		) X ORDER BY YardCode,UnitGroup, UnitCode
end
GO


