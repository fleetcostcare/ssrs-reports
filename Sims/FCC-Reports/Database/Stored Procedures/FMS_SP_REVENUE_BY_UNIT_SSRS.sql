if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_REVENUE_BY_UNIT_SSRS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_REVENUE_BY_UNIT_SSRS]
GO
set quoted_identifier off
go
/*
Written By		: Krishna Reddy 
Created Date	: August 22 2018 
Description		: SF Case:			Programmer: Krishna			Date: 08/22/2018
				  New report to show Equipment Acquisitions Analysis.
Modified Date	: 	
exec FMS_SP_REVENUE_BY_UNIT_SSRS 'north', 'Q2', 2018

*/

CREATE PROCEDURE dbo.FMS_SP_REVENUE_BY_UNIT_SSRS (@Company VARCHAR(15), @YearQuarter varchar(2), @Year int)    
AS
BEGIN
		declare @stmt varchar(max), @ds datetime, @de datetime
		
		set @ds = '01/01/'+CAST(@Year as varchar(4))
		
		if @YearQuarter = 'Q1'
		begin
			set @de = '03/31/'+CAST(@Year as varchar(4))
		end
		else if @YearQuarter = 'Q2'
		begin
			--set @ds = '04/01/'+CAST(@Year as varchar(4))
			set @de = '06/30/'+CAST(@Year as varchar(4))
		end
		else if @YearQuarter = 'Q3'
		begin
			--set @ds = '07/01/'+CAST(@Year as varchar(4))
			set @de = '08/31/'+CAST(@Year as varchar(4))
		end
		else if @YearQuarter = 'Q4'
		begin
			--set @ds = '09/01/'+CAST(@Year as varchar(4))
			set @de = '12/31/'+CAST(@Year as varchar(4))
		end
	 
		SELECT UnitCode, Description, ActiveDate, OriginalCost, 
		Revenue, EstOperCost = Revenue * .30, Single = 'S',
		Net = Revenue-(Revenue * .30),
		ROI = (isnull((Revenue-(Revenue * .30)),0)/case when OriginalCost=0 then 1 else OriginalCost end)/100.0
		FROM 
			(SELECT FU.UnitCode,
			Description = (SELECT Description FROM FMS_UNIT_TYPES FUT WHERE FUT.UnitTypeID=FU.UnitTypeID),
			ActiveDate = FU.DatePurchased, OriginalCost=isnull(FU.PurchasePrice,0),
			Revenue = (SELECT SUM(Amount) FROM FMS_VW_EQUIP_REV FVER WHERE FVER.RptName = 'Revenue' and FVER.UnitCode=FU.UnitCode and FVER.InvDate between @ds and @de)
			FROM FMS_UNITS FU
			WHERE (FU.CompanyCode = @Company OR  FU.CompanyCode  in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Company = '_All' then dbo.FMS_FN_LOGGED_USER() else '' end,'C'))) 
			and Year(FU.DatePurchased) >= @Year-2
			) X	 
			order by Year(ActiveDate) desc, UnitCode 
END
GO

