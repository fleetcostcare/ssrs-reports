
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_ProfitRevenueSummary_SSRS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_ProfitRevenueSummary_SSRS]
GO

/*
Written By		: Krishna Reddy 
Created Date	: March 06 2006	
Description		: To show the profit revenue summary details based on the different types of options selected 
				  SF Case# 9916		Programmer: Krishna 	Date: 08/20/2018
					New report for FCC SSRS
				  SF Case# 10140		Programmer: Krishna 	Date: 09/11/2018
					Passing Username parameter and checking security for company and Yard with that User.
				   
Modified Date	: 

exec FMS_SP_ProfitRevenueSummary_SSRS '1/1/2016','12/12/2016', 'north', '_Allyards'
*/


CREATE PROCEDURE DBO.FMS_SP_ProfitRevenueSummary_SSRS (@UserName varchar(20), @BegDt datetime,  @EndDt datetime,  @Companycode varchar(15),  @Yard varchar(max))
AS
BEGIN

	
	 
		DECLARE @GroupNumb char(2), @PayRateAccess bit --, @UserName varchar(20)
		--set @UserName = (select dbo.FMS_FN_LOGGED_USER())
	select @GroupNumb=case when FUO.AllGroups = 1 then null else FU.GroupNumb end FROM FMS_USERS FU,  
				FMS_USER_OPTIONS FUO WHERE  FU.UserID = FUO.USERID and FUO.UserName = @UserName

	SELECT JobCode, YardCode= isnull(YardCode,''), CustCode, YardName,  MatOverHead, RevAmount, EquipAmt, LabActHrs, CompanyName, CustName, LabAct, LaborOverHeadAmt = LaborBurdenAmt + LaborWcAmt + LaborUnionAmt, MatAct, OverHead = LaborBurdenAmt + LaborWcAmt + LaborUnionAmt + MatOverHead, LaborBurdenAmt, LaborWcAmt, LaborUnionAmt, CompanyCode, GroupNumb,  DBCustName, WoExpenses  
	FROM (SELECT top 100 PERCENT FJM.JobCode, FJM.DeptCode, FJM.SPersonCode, FJM.YardCode, FJM.CustCode,
		YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM with (nolock) WHERE FYM.YardCode = FJM.YardCode),
		MatOverHead = (SELECT isnull(sum(FMT.BurdenAmt),0) FROM FMS_MAT_TRANS FMT with (nolock) WHERE FMT.JobCode = FJM.JobCode AND FMT.TransDate BETWEEN  @BegDt AND  @EndDt),
		RevAmount = (SELECT isnull(sum(FID.ActAmount),0) FROM FMS_INVOICE_DETAILS FID with (nolock) , FMS_INVOICE_MASTER FIM with (nolock) WHERE FIM.InvoiceCode = FID.InvoiceCode AND FIM.InvDate BETWEEN @BegDt AND  @EndDt AND (FID.JobCode = FJM.JobCode)), 
		EquipAmt = (SELECT isnull(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) WHERE FID.JobCode = FJM.JobCode AND ((FID.DateStart BETWEEN @BegDt AND  @EndDt) OR FID.DateStart is null)), 
		LabActHrs = (SELECT isnull(sum(WorkedHrs),0) FROM FMS_LABOR_DETAILS FLD with (nolock) , FMS_CRAFTS FC with (nolock) WHERE FLD.CraftCode = FC.CraftCode AND (FC.PayType = 'R' OR FC.PayType = 'O' OR FC.PayType = 'D') AND FLD.JobCode = FJM.JobCode AND FLD.WorkDate BETWEEN @BegDt AND  @EndDt ), 
		CompanyName = (SELECT FCO.Name FROM FMS_COMPANIES FCO with (nolock) WHERE FCO.CompanyCode=FJM.CompanyCode), 
		CustName = (SELECT FC.Name1 FROM FMS_CUSTOMERS FC with (nolock) WHERE FC.CustCode = FJM.CustCode AND FC.GroupNumb = FJM.GroupNumb), 
		LabAct = (SELECT isnull(sum(Amount),0) FROM FMS_LABOR_DETAILS FLD with (nolock) WHERE FLD.JobCode = FJM.JobCode AND FLD.WorkDate BETWEEN @BegDt AND  @EndDt),
		MatAct = (SELECT isnull(SUM(Amount),0) FROM FMS_MAT_TRANS FMT with (nolock) WHERE FMT.JobCode = FJM.JobCode AND FMT.TransDate BETWEEN @BegDt AND  @EndDt), 
		LaborBurdenAmt = (SELECT isnull(SUM(FLD.BurdenAmt),0) FROM FMS_LABOR_DETAILS FLD with (nolock) WHERE FLD.JobCode = FJM.JobCode AND FLD.WorkDate BETWEEN @BegDt AND  @EndDt), 
		LaborWcAmt = (SELECT isnull(SUM(FLD.WcAmt),0) FROM FMS_LABOR_DETAILS FLD with (nolock) WHERE FLD.JobCode = FJM.JobCode AND FLD.WorkDate BETWEEN @BegDt AND  @EndDt), 
		LaborUnionAmt = (SELECT isnull(SUM(FLD.UnionAmt),0) FROM FMS_LABOR_DETAILS FLD with (nolock) WHERE FLD.JobCode = FJM.JobCode AND FLD.WorkDate BETWEEN @BegDt AND  @EndDt), 
		FJM.CompanyCode, FJM.GroupNumb,
		DBCustName = (SELECT Custname from fms_db_identity), 
		WoExpenses = (select ISNULL(sum(Amount),0) from fms_mat_trans FMT, fms_workorder_master fwm where fwm.WoCode=FMT.Wocode and FWM.JobCode = FJM.JobCode) 
					+ (SELECT ISNULL(sum(Amount),0) FROM FMS_LABOR_DETAILS FLD, FMS_WORKORDER_MASTER FWM WHERE FWM.WoCode=FLD.WoCode AND FWM.JobCode = FJM.JobCode) 
		FROM FMS_JOB_MASTER FJM with (nolock) 
		WHERE (FJM.CompanyCode = @CompanyCode Or isnull(FJM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @CompanyCode = '_All' then @UserName else '' end,'C')) or @UserName = '_All') 
		and (FJM.YardCode in (select Value from dbo.fms_fn_StringSplit (@Yard, ',')) Or isnull(FJM.YardCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Yard = '_All' then @UserName else '' end,'Y')) or @UserName = '_All') 
		and FJM.GroupNumb = isnull(@GroupNumb,FJM.GroupNumb) 
		 ) X 
		  where ((RevAmount <> 0) or (LabActHrs <> 0) or (MatAct <> 0) or (LabAct <> 0) or (EquipAmt <> 0) or (((LaborBurdenAmt + LaborWcAmt + LaborUnionAmt + MatOverHead) + MatOverHead) <> 0)) 

		  ORDER BY CompanyName, JobCode 
	
	 
END
GO