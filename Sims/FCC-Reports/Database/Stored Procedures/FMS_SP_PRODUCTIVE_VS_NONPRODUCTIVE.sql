
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_PRODUCTIVE_VS_NONPRODUCTIVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_PRODUCTIVE_VS_NONPRODUCTIVE]
GO
set quoted_identifier off
go
/*
Written By		: Krishna Reddy 
Created Date	: August 17 2018 
Description		: SF Case:10038			Programmer: Krishna			Date: 08/17/2018
				  New report to show Variance details .
Modified Date	: 

exec FMS_SP_PRODUCTIVE_VS_NONPRODUCTIVE 'north', '_All', 08,2018

*/

CREATE PROCEDURE dbo.FMS_SP_PRODUCTIVE_VS_NONPRODUCTIVE (@CompanyCode VARCHAR(15), @YardCode varchar(max)='_All', @Month int, @Year int)    
AS
BEGIN
		SELECT FLD.WorkDate, 
		Employee = (SELECT EmployeeName FROM FMS_VW_EMPLOYEE_NAME FVEN WHERE FVEN.EmpID=FE.EmpID and FVEN.GroupNumb=FE.GroupNumb),
		FCO.Name, FE.CompanyCode, FLD.WoCode, FLD.JobCode, FLD.Amount, 
		FLD.ActivityCode, 
		Chargeable = case when FLD.JobCode is not null  OR FLD.WoCode is not null then 1 else 0 end,
		FLD.ClassCode, Classification=(SELECT Description FROM FMS_CLASSIFICATIONS FCC WHERE FCC.ClassCode = FE.ClassCode),
		JobOrWO = Case when FLD.JobCode is not null then 'J' when FLD.WoCode is not null then 'W' else '' end,
		FLD.EmpID, 
		FLD.CraftCode,
		CraftDesc = (select Description FROM FMS_CRAFTS FC WHERE FC.CraftCode=FLD.CraftCode),
		FLD.UnitCode, FLD.WorkedHrs, FLD.Rate, FLD.Amount
		FROM FMS_LABOR_DETAILS FLD, FMS_EMPLOYEES FE, FMS_COMPANIES FCO
		WHERE  FE.EmpID=FLD.EmpID and FE.GroupNumb=FLD.GroupNumb
		and FE.CompanyCode=FCO.CompanyCode
		and (FE.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
		and (FE.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)		
END
GO
