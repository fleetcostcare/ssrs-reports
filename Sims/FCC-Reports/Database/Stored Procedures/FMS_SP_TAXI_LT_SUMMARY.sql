if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_TAXI_LT_SUMMARY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_TAXI_LT_SUMMARY]
GO
 
/*
Written By		: Krishna Reddy 
Created Date	: January 11 2019 
Description		: SF Case: 11313			Programmer: Krishna			Date: 01/11/2019
				  LT Variance report with Measure Conversion
Modified Date	: 	

exec FMS_SP_TAXI_LT_SUMMARY '_All' , 11,2018
*/

CREATE  PROCEDURE [dbo].[FMS_SP_TAXI_LT_SUMMARY] (@YardCode varchar(max)='_All', @Month int, @Year int)    
AS
BEGIN
		declare @stmt varchar(max)
	 

		SELECT  Type,
				YardCode, Branch, Salesperson, 
				Class, BillingInformation, Frequency, Measure_YN, Qty, ActRate, 
				ActualExt, 
				ListExt,
				Variance = ActualExt - ListExt,
				VariancePerc = (( ActualExt - ListExt) / CASE WHEN ListExt = 0 THEN 1 ELSE ListExt END), -- * 100.0,
				BillingCodeID,
				(select top 1 ConversionToHours from SimsReporting..Measure_Conversion where DisplayValue in (coalesce(BC_Day, BC_Week, BC_Month))),
				Helper, BC_Hours, BC_Day, BC_Week, BC_Month, 
				ListRate = case when ListRate = 0 then ActRate else ListRate end, -- if no list rate get act rate , but coming to calcualtion of List Ext if list rate not exists done multiple with the conversion hours. else multiple.
				UnitCode, OnRentDate
		FROM
			(

				SELECT  Type= case when coalesce(BC_Hours, BC_Day, BC_Week, BC_Month)= 'Hour' then 'Taxi' 
								   when coalesce(BC_Hours, BC_Day, BC_Week, BC_Month) in ('Day', 'Week', 'Month') or BillingInformation like '%Hoist- Bare Rental%' Or  BillingInformation like '%Tower Crane Bare Rental%' Or  BillingInformation like '%Tower Crane Operated%'  then 'Long term' 
								   else 'Nothing' end,
						YardCode, Branch, Salesperson, 
						Class, BillingInformation, Qty=ActQty, ActRate, 
						ActualExt =   ActAmount, 
						ListExt = case when ListRate = 0  then -- Apply Act Rate
													case when coalesce(BC_Day, BC_Week, BC_Month) in ('Day', 'Week', 'Month') and Measure_YN <>'Y'
																then  (select top 1 ConversionToHours from SimsReporting..Measure_Conversion where DisplayValue in (coalesce(BC_Day, BC_Week, BC_Month))) * Helper * ActRate 
																else Helper * ActRate  -- Hourly here
														end
										else Helper * ListRate -- List rate <> 0 here
								 end,
						BillingCodeID, Helper,Frequency,  BC_Hours, BC_Day, BC_Week, BC_Month, ListRate, UnitCode, OnRentDate, Measure_YN
				FROM
					(	SELECT	YardCode, Branch, Salesperson, AssistedBy, Customer, CustomerCity,
										JobName, JobCity, InvDate, InvoiceCode, JobCode, OnRentDate, UnitCode, EquipmentDesc, 
										Class, BillingInformation,
										Frequency, 
										ActQty, ActRate, ActAmount, ListRate, 
										--Variance = (ActualExt -ListExt), 
										--VariancePerc = ((ActualExt-ListExt)/CASE WHEN ListExt = 0 THEN 1 ELSE ListExt END) * 100.0,
										JobType, IsCrane, IncludeBilling, BC_Hours, BC_Day, BC_Week, BC_Month,
										Measure_YN,
										Helper =	CASE WHEN Measure_YN = 'N'
																		then CASE WHEN (SELECT ConversionFormula FROM  SimsReporting..Billing_Measure_Conversion A
																						WHERE	A.JobType=Y.JobType and BillingType=coalesce(BC_Hours, BC_Day, BC_Week, BC_Month)
																								and Frequency = Y.Frequency) = 'D'
																					  THEN ActQty / (SELECT ConversionUnits FROM  SimsReporting..Billing_Measure_Conversion A
																								WHERE	A.JobType=Y.JobType and BillingType=coalesce(BC_Hours, BC_Day, BC_Week, BC_Month) 
																									and Frequency = Y.Frequency)
																				  WHEN (SELECT ConversionFormula FROM  SimsReporting..Billing_Measure_Conversion A
																							WHERE	A.JobType=Y.JobType and BillingType=coalesce(BC_Hours, BC_Day, BC_Week, BC_Month)
																									and Frequency = Y.Frequency) = 'M'
																						THEN ActQty * (SELECT ConversionUnits FROM  SimsReporting..Billing_Measure_Conversion A
																								WHERE	A.JobType=Y.JobType and BillingType=coalesce(BC_Hours, BC_Day, BC_Week, BC_Month) 
																										and Frequency = Y.Frequency)
																				  else ActQty
																				END
															else ActQty
															end,
														 -- if not in exclude billing list do multiple else 0 end
										--BARE = case when JobType = 'Bare' then ActualExt else 0 end,
										--OPER = case when JobType = 'Oper' then ActualExt else 0 end,
										--MOBI = case when JobType = 'MOBI' then ActualExt else 0 end,
										CustCode, BillingCodeID, MeasureConv
						FROM
							(	SELECT	YardCode, Branch, Salesperson, AssistedBy, Customer, CustomerCity,
										JobName, JobCity, InvDate, InvoiceCode, JobCode, OnRentDate, UnitCode, EquipmentDesc, 
										Class, BillingInformation,
										Frequency, BC_Hours, BC_Day, BC_Week, BC_Month,
										Measure_YN = CASE WHEN (Frequency=coalesce(BC_Hours, BC_Day, BC_Week, BC_Month)) THEN 'Y' ELSE 'N' END, -- Column Q
										IncludeBilling, 
										MeasureConv = isnull((SELECT top 1 ConversionToHours FROM SimsReporting..Measure_Conversion WHERE DisplayValue= coalesce(BC_Hours, BC_Day, BC_Week, BC_Month)),0),
										--MeasureConv = isnull((SELECT ConversionToHours FROM SimsReporting..Measure_Conversion WHERE DisplayValue= Frequency),0),
										IsCrane = case when PATINDEX('%Crane%', Class)>=1 then 1 else 0 end, -- Check it is Crane or not from Unit Type
										ActQty, ActRate, ActAmount, ListRate,
										-- Variance = (ActualExt -ListExt), 
										-- VariancePerc = ((ActualExt-ListExt)/CASE WHEN ListExt = 0 THEN 1 ELSE ListExt END) * 100.0,
										CustCode, BillingCodeID, JobType
								FROM
								(
									SELECT	FIM.YardCode, Branch = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FIM.YardCode), 
											FIM.CustCode, Customer=fc.Name1, JobName=FCL.LocationName, FIM.InvDate, FIM.InvoiceCode, FIM.JobCode, FID.UnitCode, 
											EquipmentDesc = (SELECT UnitName FROM FMS_UNITS FU WHERE FU.UnitCode=FID.UnitCode),
											Class = (select Description FROM FMS_UNIT_TYPES FUT WHERE FUT.UnitTypeID= (select UnitTypeID from FMS_UNITS FU where FU.UnitCode=FID.UnitCode)),
											Frequency=  (SELECT case when Name like 'month%' then 'Month' when Name like 'Hr mini%' then 'Hour' else Name end FROM FMS_MEASURES FM WHERE FM.MeasureCode=FID.MeasureCode),
											BC_Hours = case when PATINDEX('%Hourly%', FBC.Description) >= 1 then 'Hour' end,
											BC_Day = case when PATINDEX('%Daily%', FBC.Description) >= 1 then 'Day' end,
											BC_Week = case when PATINDEX('%Weekly%', FBC.Description) >= 1 then 'Week' end,
											BC_Month = case when PATINDEX('%Monthly%', FBC.Description) >= 1 then 'Month' end,
											--BC_Month_4 = case when PATINDEX('%Monthly%', FBC.Description) >= 1 then 'Month' end,
											JobType = case when PATINDEX('%obiliza%', FBC.Description) >= 1 then 'MOBI' 
														   when PATINDEX('%Oper%', FBC.Description) >= 1 then 'Oper' 
														   else 'Bare' end,
											IncludeBilling = (select case when COUNT(*)> 0 then 0 else 1 end FROM SimsReporting..Excluded_Billing EB where EB.BillingCodeID=FID.BillingCodeID), -- Excluding Billing Codes Column AI
											Salesperson = (SELECT RTRIM(ISNULL(Lname,''))+ ', '+RTRIM(ISNULL(Fname,''))+' '+RTRIM(ISNULL(Mname,'')) FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode),
											FID.ActQty, FID.ActRate, 
											ActAmount = isnull(FID.ActAmount,0),
											ListRate= isnull(FBC.DefaultRate,0), -- case when isnull(FBC.DefaultRate,0) =0 then FID.ActRate else FBC.DefaultRate end, --coalesce(FBC.DefaultRate,FID.ActualRate,0), 
											BillingInformation = FBC.Description,
											AssistedBy = (select RTRIM(ISNULL(Fname,'')) + RTRIM(ISNULL(' ' + Mname,''))+RTRIM(ISNULL(' ' + Lname,''))  from FMS_ASSISTED_BY FAB where FAB.AssistedByCode = (select top 1 AssistedByCode FROM FMS_QUOTE_MASTER FQM WHERE FQM.QuoteNumb = (Select top 1 QuoteNumb FROM FMS_JOB_MASTER FJM where FJM.JobCode=FIM.JobCode))),
											OnRentDate = FID.DateStart, CustomerCity = FC.City, JobCity = FCL.City, FID.BillingCodeID
									FROM FMS_INVOICE_DETAILS FID
									LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID,
									FMS_INVOICE_MASTER FIM
									LEFT OUTER JOIN FMS_CUSTOMERS FC ON FC.CustCode=FIM.CustCode
									LEFT OUTER JOIN FMS_CUSTOMER_LOCATIONS FCL ON FCL.CustLocationID=FIM.CustLocationID 
									WHERE FIM.InvoiceCode=FID.InvoiceCode
									--and (FIM.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
									and (FIM.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)
									and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year
									and (FBC.Description like '%Crane%'
										or  FBC.Description like '%Truck%'
										 OR FBC.Description like '%Lift%'
										 Or (FBC.Description like '%Hoist%') -- and (FBC.Description like '%Bare%'))
										or (FBC.Description like '%tower%' and (FBC.Description like '%Bare%' or FBC.Description like '%Oper%'))
										 )
									and FID.ActAmount > 0 and FID.MeasureCode not in ('LS','EA') and FID.UnitCode is not null
								) X
							) Y
						) Z 
					) A where billinginformation not in ('Hoist- Monthly Service Agreement', 'Trailer RR') --like '275%' and type like 'long%'
					order by Branch, Class, UnitCode, OnRentDate
					--where type = 'long term'
				--where Salesperson = 'Ashmore, Jerry' and coalesce(BC_Hours, BC_Day, BC_Week, BC_Month) in ('Day', 'Week', 'Month') 
				--order by Type, BillingInformation
END
GO



 