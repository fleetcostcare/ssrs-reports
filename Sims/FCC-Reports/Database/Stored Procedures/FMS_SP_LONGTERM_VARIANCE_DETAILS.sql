USE [NexGenFMS]
GO
/****** Object:  StoredProcedure [dbo].[FMS_SP_LONGTERM_VARIANCE_DETAILS]    Script Date: 9/3/2018 11:09:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Added by Krishna - 8/27, 
exec FMS_SP_LONGTERM_VARIANCE_DETAILS '1', '_All' , 06,2018
exec FMS_SP_LONGTERM_VARIANCE_SUMMARY'1', '_All' , 06,2018

*/

ALTER PROCEDURE [dbo].[FMS_SP_LONGTERM_VARIANCE_DETAILS] (@CompanyCode VARCHAR(15), @YardCode varchar(max)='_All', @Month int, @Year int)    
AS
BEGIN
		declare @stmt varchar(max)
	 
					
		SELECT	YardCode, 
				Branch, 
				CustCode, 
				Customer, 
				JobName, 
				InvDate,  
				InvoiceCode, 
				JobCode, 
				UnitCode,
				EquipmentDesc, 
				Class, 
				Salesperson, 
				Frequency, 
				Qty, 
				ActualRate, 
				ActualExt, 
				ListRate, 
				ListExt,
				Variance = (ActualExt -ListExt), 
				VariancePerc = ((ActualExt-ListExt)/CASE WHEN ListExt = 0 THEN 1 ELSE ListExt END) * 100.0,
				AssistedBy,
				OnRentDate,
				BillingInformation,
				JobCity,
				CustomerCity,
				BillingCodeID
		FROM
		(
			SELECT	FIM.YardCode, 
					Branch = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FIM.YardCode), 
					FIM.CustCode, 
					Customer=fc.Name1, 
					JobName=FCL.LocationName, 
					FIM.InvDate, 
					FIM.InvoiceCode,
					FIM.JobCode, FID.UnitCode, 
					EquipmentDesc = (SELECT UnitName FROM FMS_UNITS FU WHERE FU.UnitCode=FID.UnitCode),
					Class = (select Description FROM FMS_UNIT_TYPES FUT WHERE FUT.UnitTypeID= (select UnitTypeID from FMS_UNITS FU where FU.UnitCode=FID.UnitCode)),
					Frequency= (SELECT Name FROM FMS_MEASURES FM WHERE FM.MeasureCode=FID.MeasureCode),
					Salesperson = (SELECT RTRIM(ISNULL(Lname,''))+ ', '+RTRIM(ISNULL(Fname,''))+' '+RTRIM(ISNULL(Mname,'')) FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode),
					Qty = FID.ActQty, 
					ActualRate = FID.ActRate, 
					ActualExt=isnull(FID.ActAmount,0),
					ListRate= case when isnull(FBC.DefaultRate,0)=0 then FID.ActRate else FBC.DefaultRate end, --isnull(FBC.DefaultRate,0), 
					ListExt= isnull(FID.ActQty,0) * (case when isnull(FBC.DefaultRate,0)=0 then FID.ActRate else FBC.DefaultRate end), --isnull(FBC.DefaultRate,0),
					BillingInformation = (select Description FROM FMS_BILLING_CODES FBC WHERE FBC.BillingCodeID =FID.BillingCodeID),
					AssistedBy = (select RTRIM(ISNULL(Fname,'')) + RTRIM(ISNULL(' ' + Mname,''))+RTRIM(ISNULL(' ' + Lname,''))  from FMS_ASSISTED_BY FAB where FAB.AssistedByCode = (select top 1 AssistedByCode FROM FMS_QUOTE_MASTER FQM WHERE FQM.QuoteNumb = (Select top 1 QuoteNumb FROM FMS_JOB_MASTER FJM where FJM.JobCode=FIM.JobCode))),
					OnRentDate = FID.DateStart,
					CustomerCity = FC.City,
					JobCity = FCL.City,
					FID.BillingCodeID
			FROM FMS_INVOICE_DETAILS FID
			LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID,
			FMS_INVOICE_MASTER FIM
			LEFT OUTER JOIN FMS_CUSTOMERS FC ON FC.CustCode=FIM.CustCode
			LEFT OUTER JOIN FMS_CUSTOMER_LOCATIONS FCL ON FCL.CustLocationID=FIM.CustLocationID 
			WHERE FIM.InvoiceCode=FID.InvoiceCode
			and (FIM.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
			and (FIM.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)
			and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year
		) X
END


 