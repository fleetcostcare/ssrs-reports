USE [NexGenFMS]
GO
/****** Object:  StoredProcedure [dbo].[FMS_SP_GL_REVENUE_SSRS]    Script Date: 2/7/2019 2:56:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO




/*
Added By - AG
Make GL Codes/Yard Branches dynamic.
Updated on 8/21, based on comments from Amy.
Description modified to display in upper case
Tower Crane GL accounts added under Rental based on Email from Amy.
*/

ALTER procedure [dbo].[FMS_SP_GL_REVENUE_SSRS] (@Company VARCHAR(15), @stdate DATETIME, @eddate DATETIME, @Yard varchar(20))
AS
BEGIN
	DECLARE @YardSegment int
	
	SET @YardSegment = (SELECT TOP 1 YardSegment FROM FMS_SYSTEM_DEFAULT)
	
	SELECT GLType=1, GLGroup=upper('General'), CompanyCode, Name, upper(Description) as Description
		, GLAcctCode, GLBranch
		, Amount = sum(Amount),
	GLBranchDesc = (select STUFF((SELECT  ',' + YardName  from FMS_YARD_MASTER where YardBranch = GLBranch order by YardName
					FOR XML PATH('')), 1, 1, '')),
	LessSales = case when GLBranch = '000' then SUM(Amount) else 0 end
	from (
	SELECT FIM.CompanyCode, FCO.Name, FGA.Description, 
	GlAcctCode = dbo.FMS_FN_ADJ_ACCT_YARD(COALESCE(FID.GlAcctCode, FBC.GlAcctCode),FIM.Yardcode, FIM.DeptCode, case when substring(system_user, len(system_user)-3, len(system_user)) = '_FCC' then substring(system_user,1,len(system_user)-4) else system_user end), 
	GLBranch = case when (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) is not null then (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) else substring(COALESCE(FID.GlAcctCode, FBC.GlAcctCode), DBO.FMS_FN_ACCT_START_POS (@YardSegment,'SA'), DBO.FMS_FN_SEG_LEN(@Yardsegment)) end, 
	Amount = FID.ActAmount
	FROM FMS_INVOICE_DETAILS FID
	LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FID.BillingCodeID = FBC.BillingCodeID
	LEFT OUTER JOIN FMS_GL_ACCOUNTS FGA ON FGA.GlAcctCode= COALESCE(FID.GlAcctCode, FBC.GlAcctCode),
	FMS_INVOICE_MASTER FIM, FMS_COMPANIES FCO
	WHERE FIM.InvoiceCode = FID.InvoiceCode and FCO.CompanyCode=FIM.CompanyCode and FID.ActAmount <> 0 
	and (FIM.CompanyCode = @Company OR isnull(FIM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Company = '_All' then dbo.FMS_FN_LOGGED_USER() else '' end,'C'))) 
	and (FIM.YardCode = @Yard OR @Yard='_All' OR FIM.YardCode IN (SELECT Value FROM [dbo].[FMS_Fn_StringSplit](@Yard, ','))) 
	and FIM.InvDate between @stdate and @EdDate) X
	GROUP BY CompanyCode, Name, Description, GLAcctCode, GLBranch
	
	union all
	
	SELECT GLType=3, GLGroup='HOIST', CompanyCode, Name, upper(Description) as Description, GLAcctCode, GLBranch, Amount = sum(Amount),
	GLBranchDesc = (select STUFF((SELECT  ',' + YardName  from FMS_YARD_MASTER where YardBranch = GLBranch order by YardName
					FOR XML PATH('')), 1, 1, '')),
	LessSales = case when GLBranch = '000' then SUM(Amount) else 0 end
	from (
	SELECT FIM.CompanyCode, FCO.Name, FGA.Description, 
	GlAcctCode = dbo.FMS_FN_ADJ_ACCT_YARD(COALESCE(FID.GlAcctCode, FBC.GlAcctCode),FIM.Yardcode, FIM.DeptCode, case when substring(system_user, len(system_user)-3, len(system_user)) = '_FCC' then substring(system_user,1,len(system_user)-4) else system_user end), 
	GLBranch = case when (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) is not null then (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) else substring(COALESCE(FID.GlAcctCode, FBC.GlAcctCode), DBO.FMS_FN_ACCT_START_POS (@YardSegment,'SA'), DBO.FMS_FN_SEG_LEN(@Yardsegment)) end, 
	Amount = FID.ActAmount
	FROM FMS_INVOICE_DETAILS FID
	LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FID.BillingCodeID = FBC.BillingCodeID
	LEFT OUTER JOIN FMS_GL_ACCOUNTS FGA ON FGA.GlAcctCode= COALESCE(FID.GlAcctCode, FBC.GlAcctCode),
	FMS_INVOICE_MASTER FIM, FMS_COMPANIES FCO
	WHERE FIM.InvoiceCode = FID.InvoiceCode and FCO.CompanyCode=FIM.CompanyCode and FID.ActAmount <> 0
	and (FIM.CompanyCode = @Company OR isnull(FIM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Company = '_All' then dbo.FMS_FN_LOGGED_USER() else '' end,'C'))) 
 	and (FIM.YardCode = @Yard OR @Yard='_All' OR FIM.YardCode IN (SELECT Value FROM [dbo].[FMS_Fn_StringSplit](@Yard, ','))) 
	and FGA.Description = 'HOIST RENTAL'
	and FIM.InvDate between @stdate and @EdDate) X
	GROUP BY CompanyCode, Name, Description, GLAcctCode, GLBranch
	
	union all
	
	SELECT GlType=2,GLGroup='CRANE', CompanyCode, Name, upper(Description)as Description, GLAcctCode, GLBranch, Amount = sum(Amount),
	GLBranchDesc = (select STUFF((SELECT  ',' + YardName  from FMS_YARD_MASTER where YardBranch = GLBranch order by YardName
					FOR XML PATH('')), 1, 1, '')),
	LessSales = case when GLBranch = '000' then SUM(Amount) else 0 end
	from (
	SELECT FIM.CompanyCode, FCO.Name, FGA.Description, 
	GlAcctCode = dbo.FMS_FN_ADJ_ACCT_YARD(COALESCE(FID.GlAcctCode, FBC.GlAcctCode),FIM.Yardcode, FIM.DeptCode, case when substring(system_user, len(system_user)-3, len(system_user)) = '_FCC' then substring(system_user,1,len(system_user)-4) else system_user end), 
	GLBranch = case when (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) is not null then (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) else substring(COALESCE(FID.GlAcctCode, FBC.GlAcctCode), DBO.FMS_FN_ACCT_START_POS (@YardSegment,'SA'), DBO.FMS_FN_SEG_LEN(@Yardsegment)) end, 
	Amount = FID.ActAmount
	FROM FMS_INVOICE_DETAILS FID
	LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FID.BillingCodeID = FBC.BillingCodeID
	LEFT OUTER JOIN FMS_GL_ACCOUNTS FGA ON FGA.GlAcctCode= COALESCE(FID.GlAcctCode, FBC.GlAcctCode),
	FMS_INVOICE_MASTER FIM, FMS_COMPANIES FCO
	WHERE FIM.InvoiceCode = FID.InvoiceCode and FCO.CompanyCode=FIM.CompanyCode and FID.ActAmount <> 0
	and (FIM.CompanyCode = @Company OR isnull(FIM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Company = '_All' then dbo.FMS_FN_LOGGED_USER() else '' end,'C'))) 
	and (FIM.YardCode = @Yard OR @Yard='_All' OR FIM.YardCode IN (SELECT Value FROM [dbo].[FMS_Fn_StringSplit](@Yard, ','))) 
	and FGA.Description IN ('CRANE RENTAL BARE','CRANE RENTAL OPERATED')
	and FIM.InvDate between @stdate and @EdDate) X
	GROUP BY CompanyCode, Name, Description, GLAcctCode, GLBranch
	
	union all
	
	SELECT GlType=4, GLGroup='LIFT', CompanyCode, Name, upper(Description) as Description, GLAcctCode, GLBranch, Amount = sum(Amount),
	GLBranchDesc = (select STUFF((SELECT  ',' + YardName  from FMS_YARD_MASTER where YardBranch = GLBranch order by YardName
					FOR XML PATH('')), 1, 1, '')),
	LessSales = case when GLBranch = '000' then SUM(Amount) else 0 end
	from (
	SELECT FIM.CompanyCode, FCO.Name, FGA.Description, 
	GlAcctCode = dbo.FMS_FN_ADJ_ACCT_YARD(COALESCE(FID.GlAcctCode, FBC.GlAcctCode),FIM.Yardcode, FIM.DeptCode, case when substring(system_user, len(system_user)-3, len(system_user)) = '_FCC' then substring(system_user,1,len(system_user)-4) else system_user end), 
	GLBranch = case when (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) is not null then (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) else substring(COALESCE(FID.GlAcctCode, FBC.GlAcctCode), DBO.FMS_FN_ACCT_START_POS (@YardSegment,'SA'), DBO.FMS_FN_SEG_LEN(@Yardsegment)) end, 
	Amount = FID.ActAmount
	FROM FMS_INVOICE_DETAILS FID
	LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FID.BillingCodeID = FBC.BillingCodeID
	LEFT OUTER JOIN FMS_GL_ACCOUNTS FGA ON FGA.GlAcctCode= COALESCE(FID.GlAcctCode, FBC.GlAcctCode),
	FMS_INVOICE_MASTER FIM, FMS_COMPANIES FCO
	WHERE FIM.InvoiceCode = FID.InvoiceCode and FCO.CompanyCode=FIM.CompanyCode and FID.ActAmount <> 0
	and (FIM.CompanyCode = @Company OR isnull(FIM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Company = '_All' then dbo.FMS_FN_LOGGED_USER() else '' end,'C'))) 
	and (FIM.YardCode = @Yard OR @Yard='_All' OR FIM.YardCode IN (SELECT Value FROM [dbo].[FMS_Fn_StringSplit](@Yard, ','))) 
	and FGA.Description IN ('PERSONNEL LIFT RENTAL/BARE','RT FORKLIFT RENTAL/BARE')
	and FIM.InvDate between @stdate and @EdDate) X
	GROUP BY CompanyCode, Name, Description, GLAcctCode, GLBranch
	
	union all
	
	SELECT GlType=5, GLGroup='RENTAL', CompanyCode, Name, upper(Description)as Description, GLAcctCode, GLBranch, Amount = sum(Amount),
	GLBranchDesc = (select STUFF((SELECT  ',' + YardName  from FMS_YARD_MASTER where YardBranch = GLBranch order by YardName
					FOR XML PATH('')), 1, 1, '')),
	LessSales = case when GLBranch = '000' then SUM(Amount) else 0 end
	from (
	SELECT FIM.CompanyCode, FCO.Name, FGA.Description, 
	GlAcctCode = dbo.FMS_FN_ADJ_ACCT_YARD(COALESCE(FID.GlAcctCode, FBC.GlAcctCode),FIM.Yardcode, FIM.DeptCode, case when substring(system_user, len(system_user)-3, len(system_user)) = '_FCC' then substring(system_user,1,len(system_user)-4) else system_user end), 
	GLBranch = case when (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) is not null then (SELECT YardBranch FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode=FIM.YardCode) else substring(COALESCE(FID.GlAcctCode, FBC.GlAcctCode), DBO.FMS_FN_ACCT_START_POS (@YardSegment,'SA'), DBO.FMS_FN_SEG_LEN(@Yardsegment)) end, 
	Amount = FID.ActAmount
	FROM FMS_INVOICE_DETAILS FID
	LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FID.BillingCodeID = FBC.BillingCodeID
	LEFT OUTER JOIN FMS_GL_ACCOUNTS FGA ON FGA.GlAcctCode= COALESCE(FID.GlAcctCode, FBC.GlAcctCode),
	FMS_INVOICE_MASTER FIM, FMS_COMPANIES FCO
	WHERE FIM.InvoiceCode = FID.InvoiceCode and FCO.CompanyCode=FIM.CompanyCode and FID.ActAmount <> 0
	and (FIM.CompanyCode = @Company OR isnull(FIM.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN @Company = '_All' then dbo.FMS_FN_LOGGED_USER() else '' end,'C'))) 
	and (FIM.YardCode = @Yard OR @Yard='_All' OR FIM.YardCode IN (SELECT Value FROM [dbo].[FMS_Fn_StringSplit](@Yard, ','))) 
	and FGA.Description IN ('CRANE RENTAL BARE','CRANE RENTAL OPERATED','HOIST RENTAL'
							,'CUSTOMER CONCESSIONS AND WRITE-OFFS'
							,'PERSONNEL LIFT RENTAL/BARE','RT FORKLIFT RENTAL/BARE',
							'Tower Crane Rental Bare',
							'TC Monthly Maintenance',
							'Tower Crane Operated Rental')
	and FIM.InvDate between @stdate and @EdDate) X
	GROUP BY CompanyCode, Name, Description, GLAcctCode, GLBranch 
	order by GLBranch
	
END

