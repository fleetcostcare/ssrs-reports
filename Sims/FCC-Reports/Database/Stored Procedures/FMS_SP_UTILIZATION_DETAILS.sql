 /*
Written By		: Krishna Reddy 
Created Date	:  
Description		:  SF Case 12364                Programmer: Krishna         Date: 04/09/2019
				   New SP for Utilization Details
Modified Date	: 	
exec [FMS_SP_UTILIZATION_DETAILS] 'north', '_All', '_ALL', 2,2019,3,2019

*/

alter PROCEDURE [dbo].[FMS_SP_UTILIZATION_DETAILS] (@CompanyCode VARCHAR(15), @YardCode varchar(2000)='_All',  @UnitSchedType VARCHAR(MAX), @FromMonth int, @FromYear int, @ToMonth int, @ToYear int)    
AS
BEGIN
		declare @stmt varchar(max)
	 
			--set @YardCode =  replace (@YardCode, '''','''''')  -- convert any single quote to double
			--set @YardCode =   '' + replace (@YardCode, ',',''',''') + ''  -- to convert "," to "','"
			
			--set @UnitSchedType =  replace (@UnitSchedType, '''','''''')  -- convert any single quote to double
			--set @UnitSchedType =   '' + replace (@UnitSchedType, ',',''',''') + ''  -- to convert "," to "','"
		 

		SELECT Company = Name, CompanyCode, YardCode, YardName, 
		UnitType = Description, UnitTypeID, UnitSchedType, Schedule as ScheduleType,  
		UnitCode, UnitName,
		DatePurchased, VehicleMake, VehicleModel, SerialNumber,
		JobCode, InvoiceCode, DateStart, DateEnd,
		Hours =  CurrentMonth_Hours,
		Dollars = CurrentMonth_Dollars, 
		TargetMonthHrs
		FROM
		(SELECT	FCO.Name, FU.CompanyCode,  
				FU.UnitSchedType, Schedule = (SELECT Description FROM FMS_SCHEDULE_TYPES FST WHERE FST.UnitSchedCode=FU.UnitSchedType),
				FU.UnitTypeID, FUT.Description, 
				FU.UnitCode, FU.DatePurchased,  FU.VehicleMake, FU.VehicleModel,
				FU.SerialNumber, 
				FU.YardCode, YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FU.YardCode), 
				CurrentMonth_Hours =  case when MOB.Hours is null then  FID.AllocHrs  else (FID.ActQty * MOB.Hours) end,
										
				CurrentMonth_Dollars  = FID.ActAmount,
				TargetMonthHrs = case when isnull(FUT.TargetMonthHrs,0) = 0 then 1 else FUT.TargetMonthHrs  end,
				FIM.JobCode,  FIM.InvoiceCode, FID.DateStart, FID.DateEnd, FU.UnitName
		FROM FMS_INVOICE_MASTER FIM with (nolock), 
		FMS_INVOICE_DETAILS FID with (nolock)
		LEFT OUTER JOIN SimsReporting..Mobilization_Hours MOB ON FID.BillingCodeId=MOB.BillingCodeID 
																	and MOB.UnitSchedType=FID.UnitSchedType 
																	and MOB.UnitTypeID=FID.UnitTypeID
																	and MOB.MeasureCode in ('EA', 'LS'),
		FMS_UNITS FU with (nolock) 
		LEFT OUTER JOIN FMS_UNIT_TYPES FUT with (nolock) ON FUT.UnitTypeID = FU.UnitTypeID 
		LEFT OUTER JOIN FMS_COMPANIES FCO with (nolock) ON FCO.CompanyCode = FU.CompanyCode
		WHERE FIM.InvoiceCode=FID.InvoiceCode and FU.UnitCode=FID.UnitCode
		and FU.UnitTypeID IN (SELECT UnitTypeID FROM FMS_UNIT_TYPES WHERE TrackUnit = 1) 
		and (FU.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
		and (FU.YardCode = @YardCode OR '_All'=@YardCode)
		and FU.Status = 1 
		and FIM.InvDate between (select top 1 FirstDayOfMonth from fms_date_dimension where month(Date) = @FromMonth and YEAR(Date)= @FromYear)
							and (select top 1 LastDayOfMonth from fms_date_dimension where month(Date) = @ToMonth and YEAR(Date)= @ToYear)
		--and (FU.UnitTypeID in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitTypeID,',')) OR ('-1' in (@UnitTypeID)))
		and (FU.UnitSchedType in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')) OR ('_All' in (@UnitSchedType)))
		) X 
		---GROUP BY Name, CompanyCode, YardCode, YardName, ScheduleType, Description, UnitCode, UnitName,
					
		ORDER BY Schedule, Description, UnitCode
end
GO

