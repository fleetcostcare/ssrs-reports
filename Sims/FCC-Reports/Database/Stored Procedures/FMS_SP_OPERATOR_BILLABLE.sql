
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_OPERATOR_BILLABLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_OPERATOR_BILLABLE]
GO
set quoted_identifier off
go
/*
Written By		: Krishna Reddy 
Created Date	: August 17 2018 
Description		: SF Case:	10038		Programmer: Krishna			Date: 08/17/2018
				  New report to show Variance details .
Modified Date	: 

exec FMS_SP_OPERATOR_BILLABLE 'north', '_All', 08,2018

*/

CREATE PROCEDURE dbo.FMS_SP_OPERATOR_BILLABLE (@CompanyCode VARCHAR(15), @YardCode varchar(max)='_All', @Month int, @Year int)    
AS
BEGIN

		SELECT EmpID, YardCode, Employee, JobDesc, Billable, NonBillable, Total=ISNULL(Billable,0)+ ISNULL(NonBillable,0),
		Utilized= (Billable/ CASE WHEN (Billable+NonBillable)=0 THEN 1 ELSE Billable+NonBillable end) * 100.0,
		TermDate
		FROM 
			(SELECT EmpID, YardCode, Employee, JobDesc, Billable= ISNULL(SUM(Billable),0), NonBillable=ISNULL(SUM(NonBillable),0), TermDate
			FROM 
				(SELECT FLD.EmpID, FE.YardCode, 
				Employee = (SELECT EmployeeName FROM FMS_VW_EMPLOYEE_NAME FVEN WHERE FVEN.EmpID=FE.EmpID and FVEN.GroupNumb=FE.GroupNumb),
				Billable = case when FLD.JobCode is not null or FLD.WoCode is not null then FLD.WorkedHrs else 0 end,
				NonBillable = case when FLD.JobCode is  null or FLD.WoCode is  null then FLD.WorkedHrs else 0 end,
				FE.TermDate,
				JobDesc=(SELECT Description FROM FMS_CLASSIFICATIONS FCC WHERE FCC.ClassCode = FLD.ClassCode)
				FROM FMS_LABOR_DETAILS FLD, FMS_EMPLOYEES FE
				WHERE  FE.EmpID=FLD.EmpID and FE.GroupNumb=FLD.GroupNumb
					AND (FE.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
					and (FE.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)
				) X
				group by EmpID, Employee, JobDesc, YardCode, TermDate
			) Y
END
GO

