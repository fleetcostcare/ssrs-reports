 if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_SALESPERSON_DISTRIBUTION_DETAILS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_SALESPERSON_DISTRIBUTION_DETAILS]
GO

/*
Written By		: Krishna Reddy  
Created Date	: 11/28/2018
Description		: SF Case 10878			Programmer: Krishna			Date: 11/28/2018
				  SP to get the Sales person wise Revenue details .
				  SF Case 11180			Programmer: Krishna			Date: 12/24/2018
				  Added conditon to move terminated Saleperson to *OTHERS category
				  SF Case 12358			Programmer: Krishna			Date: 04/09/2019
				  Added conditon to move terminated Saleperson to *OTHERS category
Modified Date	:             

exec FMS_SP_SALESPERSON_DISTRIBUTION_DETAILS   '1', '_All', 8, 2018
 
*/

CREATE PROCEDURE [dbo].[FMS_SP_SALESPERSON_DISTRIBUTION_DETAILS] (@CompanyCode VARCHAR(15), @YardCode varchar(max)='_All', @Month int, @Year int,@Salesperson varchar(50)='_All')    
AS
BEGIN
             
			SELECT Salesperson, AssistedBy, Branch,  YardCode, UnitCode, EquipmentDesc, Class, 
                           InvDate, InvoiceCode, 
                           TotalBilled, CustCode, Customer, JobName, CustomerCity, JobCity, JobCode
            FROM (
              SELECT  Salesperson =  CASE Branch WHEN 'SIMS' THEN 'X-' + Salesperson WHEN 'Corporate Shop' THEN '* OTHERS' ELSE CASE WHEN SalespersonStatus=0 THEN '* OTHERS' ELSE Salesperson END  END,
                           AssistedBy, Branch,  YardCode, UnitCode, EquipmentDesc, Class, 
                           InvDate, InvoiceCode, 
                           TotalBilled = ROUND(TotalBilled,0), 
                           CustCode, Customer, JobName, CustomerCity, JobCity, JobCode
              FROM (
              SELECT Salesperson = isnull((SELECT RTRIM(ISNULL(Lname,''))+ ', '+RTRIM(ISNULL(Fname,''))+' '+RTRIM(ISNULL(Mname,'')) FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode), '* OTHERS'),
					 SalespersonStatus = (SELECT Status FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode),
                                  AssistedBy = (select RTRIM(ISNULL(Fname,'')) + RTRIM(ISNULL(' ' + Mname,''))+RTRIM(ISNULL(' ' + Lname,''))  from FMS_ASSISTED_BY FAB where FAB.AssistedByCode = (select top 1 AssistedByCode FROM FMS_QUOTE_MASTER FQM WHERE FQM.QuoteNumb = (Select top 1 QuoteNumb FROM FMS_JOB_MASTER FJM where FJM.JobCode=FIM.JobCode))),
                                  Branch = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FIM.YardCode), 
                                  FIM.YardCode, 
                                  UnitCode =  (Select TOP 1 MAX(UnitCode) FROM FMS_INVOICE_DETAILS FID WHERE FIM.InvoiceCode=FID.InvoiceCode),
                                  EquipmentDesc = (SELECT UnitName FROM FMS_UNITS FU WHERE FU.UnitCode in (Select TOP 1 MAX(UnitCode) FROM FMS_INVOICE_DETAILS FID WHERE FIM.InvoiceCode=FID.InvoiceCode)),
                                  Class = (select Description FROM FMS_UNIT_TYPES FUT WHERE FUT.UnitTypeID= (select UnitTypeID from FMS_UNITS FU where FU.UnitCode in (Select TOP 1 MAX(UnitCode) FROM FMS_INVOICE_DETAILS FID WHERE FIM.InvoiceCode=FID.InvoiceCode))),
                                  FIM.InvDate, 
                                  FIM.InvoiceCode,
                                  TotalBilled = (Select SUM(FID.ActAmount) FROM FMS_INVOICE_DETAILS FID WHERE FIM.InvoiceCode=FID.InvoiceCode),
                                  FIM.CustCode, 
                                  Customer=fc.Name1, 
                                  JobName=FCL.LocationName, 
                                  CustomerCity = FC.City,
                                  JobCity = FCL.City,
                                  FIM.JobCode
                     FROM FMS_INVOICE_MASTER FIM
                     LEFT OUTER JOIN FMS_CUSTOMERS FC ON FC.CustCode=FIM.CustCode AND FC.GroupNumb=FIM.GroupNumb
                     LEFT OUTER JOIN FMS_CUSTOMER_LOCATIONS FCL ON FCL.CustLocationID=FIM.CustLocationID 
                     WHERE (FIM.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
                     and (FIM.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)
                     and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year
                     ) X
                     ) Y
                     where (Salesperson= @Salesperson or @Salesperson='_All')
                     ORDER BY 1, InvoiceCode 
END
GO


 