 if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_SALESPERSON_DISTRIBUTION_SUMMARY]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_SALESPERSON_DISTRIBUTION_SUMMARY]
GO
 
 /*
Written By		: Krishna Reddy  
Created Date	: 11/28/2018
Description		: SF Case 10878			Programmer: Krishna			Date: 11/28/2018
				  SP to get the Sales person wise Revenue Summary .
				   SF Case 11180			Programmer: Krishna			Date: 12/24/2018
				  Added conditon to move terminated Saleperson to *OTHERS category
				  SF Case 12358			Programmer: Krishna			Date: 04/09/2019
				  Added conditon to move terminated Saleperson to *OTHERS category 
Modified Date	:             

exec FMS_SP_SALESPERSON_DISTRIBUTION_SUMMARY   'north', '_All', 1, 2018
 
*/

CREATE PROCEDURE [dbo].[FMS_SP_SALESPERSON_DISTRIBUTION_SUMMARY] (@CompanyCode VARCHAR(15), @YardCode varchar(max)='_All', @Month int, @Year int)    
AS
BEGIN
              declare @PresentYTD int, @FirstDayOfPresentYear datetime , @FirstDayOfLastYear datetime, @LastDayOfLastYear datetime, @LastDayOfMonth datetime
              
              SET @FirstDayOfPresentYear = (SELECT top 1 FirstDayOfYear FROM FMS_DATE_DIMENSION WHERE Year(Date) = @Year)
              SET @FirstDayOfLastYear = (SELECT top 1 FirstDayOfYear FROM FMS_DATE_DIMENSION WHERE Year(Date) = @Year-1)
              SET @LastDayOfLastYear = (SELECT top 1 LastDayOfYear FROM FMS_DATE_DIMENSION WHERE Year(Date) = @Year-1)
              SET @LastDayOfMonth = (SELECT top 1 LastDayOfMonth FROM FMS_DATE_DIMENSION WHERE  Month(Date)= @Month and Year(Date) = @Year)
              
              --select @FirstDayOfPresentYear, @FirstDayOfLastYear, @LastDayOfLastYear, @LastDayOfMonth 
              --SELECT top 1 * FROM FMS_DATE_DIMENSION
              
              SELECT SalesGroup = CASE When LEFT(Salesperson,8)='Dispatch' THEN 'House Accounts'
                                                     When LEFT(Salesperson,2)='X-' THEN 'Equipment Sales'
                                                     --WHEN Salesperson = '* OTHERS' THEN '* OTHERS'
                                                     ELSE 'Sales Reps' END,
                           Salesperson, 
                           PresentMonth=round(isnull(SUM(PresentMonth),0),0), 
                           LastMonth= round(isnull(SUM(LastMonth),0),0),
                           YTD= round(isnull(SUM(YTD),0),0),
                           LastYTD= round(isnull(SUM(LastYTD),0),0),
                           AvgLastYear = round(isnull(SUM(LastYTD),0)/12,0)
              FROM ( 
                     SELECT  Salesperson = CASE Branch WHEN 'SIMS' THEN 'X-' + Salesperson 
													   WHEN 'Corporate Shop' THEN '* OTHERS' ELSE 
													   CASE WHEN SalespersonStatus=0 THEN '* OTHERS' ELSE Salesperson END  
											END, 
					              PresentMonth = CASE WHEN MONTH(InvDate)=@Month and YEAR(InvDate) = @Year then SUM(TotalBilled) end,
                                  LastMonth = CASE WHEN MONTH(InvDate)= (case when @Month=1 then 12 else @Month-1 end) and YEAR(InvDate) = (case when @Month=1 then @Year-1 else @Year end) then SUM(TotalBilled) end,
                                  YTD = CASE WHEN InvDate between @FirstDayOfPresentYear and @LastDayOfMonth then SUM(TotalBilled) end,
                                  LastYTD = CASE WHEN InvDate between @FirstDayOfLastYear and @LastDayOfLastYear then SUM(TotalBilled) end
                     FROM (
                                  SELECT Salesperson = ISNULL((SELECT RTRIM(ISNULL(Lname,''))+ ', '+RTRIM(ISNULL(Fname,''))+' '+RTRIM(ISNULL(Mname,'')) FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode), '* OTHERS'),
								  SalespersonStatus = (SELECT Status FROM FMS_SALES_PERSON FSP WHERE FSP.SpersonCode = FIM.SpersonCode),
                                         FIM.InvDate, 
                                         Branch = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FIM.YardCode), 
                                         TotalBilled = (Select SUM(FID.ActAmount) FROM FMS_INVOICE_DETAILS FID WHERE FIM.InvoiceCode=FID.InvoiceCode)
                                  FROM FMS_INVOICE_MASTER FIM
                                  WHERE (FIM.CompanyCode = @CompanyCode OR '_All'=@CompanyCode)
                                  and (FIM.YardCode in (select Value from dbo.fms_fn_StringSplit(@YardCode,',')) Or '_ALL'=@YardCode)
                                  and  FIM.InvDate between @FirstDayOfLastYear  and @LastDayOfMonth
                           ) X group by InvDate, Branch, Salesperson, SalespersonStatus
                     ) Y
                     group by Salesperson
                     ORDER BY 1 DESC,2 ASC
END
GO



 