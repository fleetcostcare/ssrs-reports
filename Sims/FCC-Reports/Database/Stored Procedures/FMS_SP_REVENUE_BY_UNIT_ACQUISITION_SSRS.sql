if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FMS_SP_REVENUE_BY_UNIT_ACQUISITION_SSRS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[FMS_SP_REVENUE_BY_UNIT_ACQUISITION_SSRS]
GO
set quoted_identifier off
go
/*
Written By		: Krishna Reddy 
Created Date	: 12/04/2018
Description		: SF Case: 10879			Programmer: Krishna			Date: 12/04/2018
				  New report to show Equipment Acquisitions Analysis.
				  Modified SP on 12/21/2018. Added filters UnitType, Month and Year
Modified Date	: 	
exec FMS_SP_REVENUE_BY_UNIT_ACQUISITION_SSRS 'north' 

*/

CREATE PROCEDURE [dbo].[FMS_SP_REVENUE_BY_UNIT_ACQUISITION_SSRS] (@Company VARCHAR(15), @UnitType varchar(max), @Month int, @Year int)    
AS
BEGIN
		declare @ds datetime, @de datetime
		
		--set @ds = '01/01/'+CAST(Year(GETDATE()) as varchar(4))
		--set @de = convert(varchar(10),GETDATE(),101)
	 
		set @ds = (select top 1 FirstDayOfYear FROM FMS_DATE_DIMENSION WHERE Year=@Year)
		set @de = (select top 1 LastDayOfMonth FROM FMS_DATE_DIMENSION WHERE month=@Month and Year=@Year)

		SELECT UnitCode, Description, AcquisitionDate, AcquisitionCost, 
				RevenueYTD,
				PresentYearCostRecovered = (CASE WHEN AcquisitionCost <> 0 THEN (RevenueYTD/AcquisitionCost) ELSE  1 END) ,
				ReturnPerc = ((CASE WHEN AcquisitionCost <> 0 THEN (RevenueYTD/AcquisitionCost) ELSE  1 END) ) - 
										CASE WHEN AcquisitionCost <> 0 THEN 1 else 0 end,
				RevenueLifeToDate,
				LifeToYearReturn = ((CASE WHEN AcquisitionCost <> 0 THEN (RevenueLifeToDate/AcquisitionCost) ELSE  1 END) ) -  
											CASE WHEN AcquisitionCost <> 0 THEN 1 else 0 end
		FROM (
			 SELECT FU.UnitCode,
				Description = (SELECT Description FROM FMS_UNIT_TYPES FUT WHERE FUT.UnitTypeID=FU.UnitTypeID),
				AcquisitionDate = ta.AcquisitionDate,--FU.DatePurchased,
				AcquisitionCost=  isnull(ta.AcquisitionCost,0) ,
				RevenueYTD = isnull((SELECT SUM(Amount) FROM FMS_VW_EQUIP_REV FVER WHERE FVER.RptName = 'Revenue' and FVER.UnitCode=FU.UnitCode and FVER.InvDate between @ds and @de),0),
				RevenueLifeToDate = coalesce(FU.Revenue,0) + isnull((SELECT SUM(Amount) FROM FMS_VW_EQUIP_REV FVER WHERE FVER.RptName = 'Revenue' and FVER.UnitCode=FU.UnitCode and FVER.InvDate between ISNULL(ta.AcquisitionDate, '01/01/1900') and @de),0)
				FROM FMS_UNITS FU left outer join SCI.dbo.tblFaAsset ta on FU.UnitCode = ta.AssetId
				WHERE (FU.CompanyCode = @Company OR  @Company = '_A') 
				and (FU.UnitTypeID in (select Value from dbo.fms_fn_StringSplit (@UnitType, ',')) OR '-1' in (@UnitType))
				) X	 
		order by UnitCode 
END
GO



			