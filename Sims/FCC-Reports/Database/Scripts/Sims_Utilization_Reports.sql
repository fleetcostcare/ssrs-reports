﻿
/****** Object:  StoredProcedure [dbo].[FMS_SP_UTILIZATION_BRANCHWISE]    Script Date: 5/13/2018 8:19:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


/*
Written By		: Krishna Reddy 
Created Date	:  
Description		:  
Modified Date	: 	
AG - 4/8 - Adjusted the filters, removed the function refererence for company/yards assoicated with windows id (to be added later when we integrate SSRS reports with NexGen)
exec FMS_SP_UTILIZATION_BRANCHWISE  '_All', '_All', 1, 2018, '_All','-1'

*/

CREATE PROCEDURE [dbo].[FMS_SP_UTILIZATION_BRANCHWISE] (@CompanyCode VARCHAR(15), @YardCode varchar(2000)='_All', @Month int, @Year int, @UnitSchedType VARCHAR(MAX), @UnitTypeID integer)    
AS
BEGIN
		declare @stmt varchar(max), @LastMonth int, @LastMonthYear int
	 
			--set @YardCode =  replace (@YardCode, '''','''''')  -- convert any single quote to double
			--set @YardCode =   '' + replace (@YardCode, ',',''',''') + ''  -- to convert "," to "','"
			
			--set @UnitSchedType =  replace (@UnitSchedType, '''','''''')  -- convert any single quote to double
			--set @UnitSchedType =   '' + replace (@UnitSchedType, ',',''',''') + ''  -- to convert "," to "','"
		 
		  set @LastMonth = case when @Month = 1 then 12 else @Month-1 end
		  set @LastMonthYear = case when @Month = 1 then @Year-1 else @Year End

		SELECT Company = Name, CompanyCode,
		YardCode,
		YardName,
		UnitSchedType, Schedule,  UnitTypeID, UnitType = Description,
		UnitCode,
		UnitName,
		CASE WHEN LEFT(UnitCode,1) like '[a-z]%' THEN 'Crane' else 'Lift' end as UnitGroup,
		DatePurchased, 
		VehicleMake, VehicleModel, SerialNumber,
		CurrentMonth_Dollars=round(CurrentMonth_Dollars,0), CurrentMonth_Hours, 
		Current_Util = round((CurrentMonth_Hours / TargetMonthHrs) * 100,1),
		LastMonth_Dollars=round(LastMonth_Dollars,0), LastMonth_Hours, 
		LastMonth_Util = round((LastMonth_Hours / TargetMonthHrs) * 100,1)
		FROM
		(SELECT	FCO.Name, 
				FU.CompanyCode,  
				FU.UnitSchedType, 
				Schedule = FST.Description,
				FU.UnitTypeID, FUT.Description, 
				FU.UnitCode, FU.DatePurchased,  
				FU.UnitName,
				FU.VehicleMake, FU.VehicleModel,
				FU.SerialNumber, 
				FU.YardCode, YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FU.YardCode), 
				CurrentMonth_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_DETAILS FID with (nolock)  
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year), 
				CurrentMonth_Dollars  = (SELECT ISNULL(SUM(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) 
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year), 
				
				LastMonth_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_DETAILS FID with (nolock)  
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = @LastMonth and  Year(FID.DateStart) = @LastMonthYear), 
				LastMonth_Dollars  = (SELECT ISNULL(SUM(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) 
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = @LastMonth and  Year(FID.DateStart) = @LastMonthYear),  
										
			 TargetMonthHrs = case when isnull(FUT.TargetMonthHrs,0) = 0 then 1 else FUT.TargetMonthHrs  end
		FROM FMS_UNITS FU with (nolock) 
		LEFT OUTER JOIN FMS_UNIT_TYPES FUT with (nolock) ON FUT.UnitTypeID = FU.UnitTypeID 
		LEFT OUTER JOIN FMS_COMPANIES FCO with (nolock) ON FCO.CompanyCode = FU.CompanyCode 
		LEFT OUTER JOIN FMS_SCHEDULE_TYPES FST with (nolock) ON FST.UnitSchedCode=FU.UnitSchedType
		where FU.UnitTypeID IN (SELECT UnitTypeID FROM FMS_UNIT_TYPES WHERE TrackUnit = 1) 
		--and (FU.CompanyCode = @CompanyCode  OR isnull(FU.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_AllCompany' in (@CompanyCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'C')))
		--and (FU.YardCode in (@YardCode) OR isnull(FU.YardCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_AllYards' in (@YardCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'Y')))
		and (FU.CompanyCode = @CompanyCode  OR @CompanyCode='_All')
		and (FU.YardCode in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@YardCode,',')) OR @YardCode='_All')
		and FU.Status = 1 
		and (FST.Description like '%Crane%' or FST.Description like '%Lift%')
		and (FU.UnitTypeID in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitTypeID,',')) OR ('-1' in (@UnitTypeID)))
		and (FU.UnitSchedType in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')) OR ('_All' in (@UnitSchedType)))
		) X ORDER BY YardCode,UnitGroup, UnitCode
end
GO



/****** Object:  StoredProcedure [dbo].[FMS_SP_UTILIZATION]    Script Date: 5/13/2018 8:31:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

/*
Written By		: Krishna Reddy 
Created Date	:  
Description		:  
Modified Date	: 	
exec FMS_SP_UTILIZATION '_All', '_All', 1, 2018, '_All','-1'
Modified by AG - for "_All" and removed Company, Yard and Department based on Logged on user - 4/9.
*/

CREATE PROCEDURE [dbo].[FMS_SP_UTILIZATION] (@CompanyCode VARCHAR(15), @YardCode varchar(2000)='_All', @Month int, @Year int, @UnitSchedType VARCHAR(MAX), @UnitTypeID integer)    
AS
BEGIN
		declare @stmt varchar(max)
	 
			--set @YardCode =  replace (@YardCode, '''','''''')  -- convert any single quote to double
			--set @YardCode =   '' + replace (@YardCode, ',',''',''') + ''  -- to convert "," to "','"
			
			--set @UnitSchedType =  replace (@UnitSchedType, '''','''''')  -- convert any single quote to double
			--set @UnitSchedType =   '' + replace (@UnitSchedType, ',',''',''') + ''  -- to convert "," to "','"
		 
		 --select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')

		SELECT Company = Name, CompanyCode, YardCode, YardName, UnitSchedType, Schedule as ScheduleType,  UnitTypeID, UnitType = Description,
		UnitCode, DatePurchased, VehicleMake, VehicleModel, SerialNumber,
		CurrentMonth_Dollars=round(CurrentMonth_Dollars,0), CurrentMonth_Hours, Current_Util = round((CurrentMonth_Hours / TargetMonthHrs) * 100,1),
		LastMonth_Dollars=round(LastMonth_Dollars,0), LastMonth_Hours, LastMonth_Util = round((LastMonth_Hours / TargetMonthHrs) * 100,1),
		Year_Dollars=round(Year_Dollars,0), Year_Hours, Year_Util = round((Year_Hours / (TargetMonthHrs*@Month)) * 100,1),
		LastYear_Dollars=round(LastYear_Dollars,0), LastYear_Hours, LastYear_Util = round((LastYear_Hours / (TargetMonthHrs*12)) * 100,1),
		TargetMonthHrs
		FROM
		(SELECT	FCO.Name, FU.CompanyCode,  
				FU.UnitSchedType, Schedule = (SELECT Description FROM FMS_SCHEDULE_TYPES FST WHERE FST.UnitSchedCode=FU.UnitSchedType),
				FU.UnitTypeID, FUT.Description, 
				FU.UnitCode, FU.DatePurchased,  FU.VehicleMake, FU.VehicleModel,
				FU.SerialNumber, 
				FU.YardCode, YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FU.YardCode), 
				CurrentMonth_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_DETAILS FID with (nolock)  
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year), 
				CurrentMonth_Dollars  = (SELECT ISNULL(SUM(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) 
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year), 
		
				LastMonth_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_DETAILS FID with (nolock)  
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = case when @Month = 1 then 12 else @Month-1 end and  Year(FID.DateStart) = case when @Month = 1 then @Year-1 else @Year End),  
				LastMonth_Dollars  = (SELECT ISNULL(SUM(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) 
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) = case when @Month = 1 then 12 else @Month-1 end and  Year(FID.DateStart) = case when @Month = 1 then @Year-1 else @Year End),  
										
				 Year_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_DETAILS FID with (nolock)  
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) between 1 and @Month and Year(FID.DateStart) = @Year), 
				Year_Dollars  = (SELECT ISNULL(SUM(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) 
										WHERE FID.UnitCode = FU.UnitCode and month(FID.DateStart) between 1 and @Month and Year(FID.DateStart) = @Year), 
										
				 LastYear_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_DETAILS FID with (nolock)  
										WHERE FID.UnitCode = FU.UnitCode and Year(FID.DateStart) = @Year-1), 
				 LastYear_Dollars  = (SELECT ISNULL(SUM(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock) 
										WHERE FID.UnitCode = FU.UnitCode and Year(FID.DateStart) = @Year-1), 
				 TargetMonthHrs = case when isnull(FUT.TargetMonthHrs,0) = 0 then 1 else FUT.TargetMonthHrs  end
		FROM FMS_UNITS FU with (nolock) 
		LEFT OUTER JOIN FMS_UNIT_TYPES FUT with (nolock) ON FUT.UnitTypeID = FU.UnitTypeID 
		LEFT OUTER JOIN FMS_COMPANIES FCO with (nolock) ON FCO.CompanyCode = FU.CompanyCode 
		where FU.UnitTypeID IN (SELECT UnitTypeID FROM FMS_UNIT_TYPES WHERE TrackUnit = 1) 
		and (FU.CompanyCode = @CompanyCode  OR @CompanyCode='_All')
		and (FU.YardCode in (@YardCode) OR @YardCode='_All')
		--isnull(FU.YardCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_All' in (@YardCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'Y')))
		and FU.Status = 1 
		and (FU.UnitTypeID in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitTypeID,',')) OR ('-1' in (@UnitTypeID)))
		and (FU.UnitSchedType in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')) OR ('_All' in (@UnitSchedType)))
		) X 
		ORDER BY ScheduleType,UnitType
end
GO


/****** Object:  StoredProcedure [dbo].[FMS_SP_UTILIZATION_REVENUETYPE]    Script Date: 5/13/2018 8:37:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

/*
Written By		: Krishna Reddy 
Created Date	:  
Description		:  
Modified Date	: 	
-- exec FMS_SP_UTILIZATION_REVENUETYPE  'north', '_AllYards', 3, 2006, '_All','-1'
Modified by AG - for "_All" and removed Company, Yard and Department based on Logged on user - 4/9. 
*/
  
CREATE PROCEDURE [dbo].[FMS_SP_UTILIZATION_REVENUETYPE] (@CompanyCode VARCHAR(15), @YardCode varchar(2000)='_AllYards', @Month int, @Year int, @UnitSchedType VARCHAR(MAX), @UnitTypeID integer)      
AS  
BEGIN  
  declare @stmt varchar(max) , @LastMonth int, @LastMonthYear int
    
	--set @YardCode =  replace (@YardCode, '''','''''')  -- convert any single quote to double  
	--set @YardCode =   '' + replace (@YardCode, ',',''',''') + ''  -- to convert "," to "','"  
     
	--set @UnitSchedType =  replace (@UnitSchedType, '''','''''')  -- convert any single quote to double  
	--set @UnitSchedType =   '' + replace (@UnitSchedType, ',',''',''') + ''  -- to convert "," to "','"  
     
    set @LastMonth = case when @Month = 1 then 12 else @Month-1 end
    set @LastMonthYear = case when @Month = 1 then @Year-1 else @Year End
     
    
	  SELECT Company = Name, CompanyCode, 
	  YardCode,
	  YardName,
	  UnitSchedType, Schedule,  UnitTypeID, UnitType = Description,  
	  UnitCode, DatePurchased, VehicleMake, VehicleModel, SerialNumber,  
	   CurrentMonth_Bare=round(CurrentMonth_Bare,0), CurrentMonth_Operated=round(CurrentMonth_Operated,0), CurrentMonth_Mobi=round(CurrentMonth_Mobi,0),   
	   LastMonth_Bare=round(LastMonth_Bare,0), LastMonth_Operated=round(LastMonth_Operated,0), LastMonth_Mobi=round(LastMonth_Mobi,0), 
	   Year_Bare=round(Year_Bare,0), Year_Operated=round(Year_Operated,0), Year_Mobi=round(Year_Mobi,0)
	  FROM  
	  (SELECT FCO.Name, FU.CompanyCode,    
		FU.UnitSchedType, Schedule = (SELECT Description FROM FMS_SCHEDULE_TYPES FST WHERE FST.UnitSchedCode=FU.UnitSchedType),  
		FU.UnitTypeID, FUT.Description,   
		FU.UnitCode, FU.DatePurchased,  FU.VehicleMake, FU.VehicleModel,  
		FU.SerialNumber,   
		FU.YardCode, YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FU.YardCode),   
		CurrentMonth_Bare  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='B' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year), 
		CurrentMonth_Operated  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='O' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year), 
		CurrentMonth_Mobi  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='N' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) = @Month and  Year(FID.DateStart) = @Year),
												
		LastMonth_Bare  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='B' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) = @LastMonth and  Year(FID.DateStart) = @LastMonthYear),
		LastMonth_Operated  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='O' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) = @LastMonth and  Year(FID.DateStart) = @LastMonthYear),
		LastMonth_Mobi  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='N' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) = @LastMonth and  Year(FID.DateStart) = @LastMonthYear),
												
		Year_Bare  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='B' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) between 1 and @Month and Year(FID.DateStart) = @Year),
		Year_Operated  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='O' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) between 1 and @Month and Year(FID.DateStart) = @Year),
		Year_Mobi  =  (SELECT ISNULL(sum(FID.AllocExpAmt),0) FROM FMS_INVOICE_DETAILS FID with (nolock)
																  LEFT OUTER JOIN FMS_BILLING_CODES FBC ON FBC.BillingCodeID=FID.BillingCodeID  
										WHERE FBC.RentalType='N' and FID.UnitCode = FU.UnitCode 
												and month(FID.DateStart) between 1 and @Month and Year(FID.DateStart) = @Year)	  
	  FROM FMS_UNITS FU with (nolock)  
	  LEFT OUTER JOIN FMS_UNIT_TYPES FUT with (nolock) ON FUT.UnitTypeID = FU.UnitTypeID   
	  LEFT OUTER JOIN FMS_COMPANIES FCO with (nolock) ON FCO.CompanyCode = FU.CompanyCode   
	  where FU.UnitTypeID IN (SELECT UnitTypeID FROM FMS_UNIT_TYPES WHERE TrackUnit = 1)   
	  and (FU.CompanyCode = @CompanyCode  OR @CompanyCode='_All')
	  and (FU.YardCode in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@YardCode,',')) OR @YardCode='_All')
	  and FU.Status = 1   
	  and (FU.UnitTypeID in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitTypeID,',')) OR ('-1' in (@UnitTypeID)))  
	  and (FU.UnitSchedType in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')) OR ('_All' in (@UnitSchedType)))  
	  ) X ORDER BY Schedule, Description, UnitCode
END  
GO


/****** Object:  UserDefinedFunction [dbo].[FMS_FN_STRINGSPLIT]    Script Date: 5/13/2018 8:40:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE FUNCTION [dbo].[FMS_FN_STRINGSPLIT]   (@String  VARCHAR(MAX), @Separator CHAR(1))  
RETURNS @RESULT TABLE(Value VARCHAR(MAX))  
AS  
BEGIN       
 DECLARE @SeparatorPosition INT ,  @Value VARCHAR(MAX), @StartPosition INT  
set @SeparatorPosition 	= CHARINDEX(@Separator,@String)
  set @StartPosition = 1 
 IF @SeparatorPosition = 0    
  BEGIN  
   INSERT INTO @RESULT VALUES(@String)  
   RETURN  
  END  
       
 SET @String = @String + @Separator  
 WHILE @SeparatorPosition > 0  
  BEGIN  
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)  
   
   IF( @Value <> ''  )   
    INSERT INTO @RESULT VALUES(@Value)  
     
   SET @StartPosition = @SeparatorPosition + 1  
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)  
  END      
       
 RETURN  
END

GO




