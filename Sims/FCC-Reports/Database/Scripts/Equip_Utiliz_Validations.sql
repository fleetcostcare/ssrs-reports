Declare @Year int=2018,
@Month int = 6

SELECT			Inv.InvoiceCode,
				Inv.UnitCode,
				Inv.AllocHrs,
				Inv.ActAmount,
				FCO.Name, 
				FU.CompanyCode,  
				FU.UnitSchedType, 
				Schedule = FST.Description,
				FU.UnitTypeID, FUT.Description, 
				FU.UnitCode, FU.DatePurchased,  
				FU.UnitName,
				FU.VehicleMake, FU.VehicleModel,
				FU.SerialNumber, 
				FU.YardCode, 
				YardName = (SELECT YardName FROM FMS_YARD_MASTER FYM WHERE FYM.YardCode = FU.YardCode), 

				--CurrentMonth_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM FMS_INVOICE_MASTER FIM with (nolock)
				--						INNER JOIN FMS_INVOICE_DETAILS FID with (nolock) ON FIM.InvoiceCode =FID.InvoiceCode  
				--						WHERE FID.UnitCode = FU.UnitCode and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year), 

				--CurrentMonth_Dollars  = (SELECT ISNULL(SUM(FID.ActAmount),0) FROM FMS_INVOICE_MASTER FIM with (nolock)
				--						INNER JOIN FMS_INVOICE_DETAILS FID with (nolock) ON FIM.InvoiceCode =FID.InvoiceCode  
				--						WHERE FID.UnitCode = FU.UnitCode and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year), 
				
				--LastMonth_Hours = (SELECT ISNULL(sum(FID.AllocHrs),0) FROM  FMS_INVOICE_MASTER FIM with (nolock)
				--						INNER JOIN FMS_INVOICE_DETAILS FID with (nolock) ON FIM.InvoiceCode =FID.InvoiceCode  
				--						WHERE FID.UnitCode = FU.UnitCode and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year),   
										

				--LastMonth_Dollars  = (SELECT ISNULL(SUM(FID.ActAmount),0) FROM FMS_INVOICE_MASTER FIM with (nolock)
				--						INNER JOIN FMS_INVOICE_DETAILS FID with (nolock) ON FIM.InvoiceCode =FID.InvoiceCode  
				--						WHERE FID.UnitCode = FU.UnitCode and month(FIM.InvDate) = @Month and  Year(FIM.InvDate) = @Year), 
										
			 TargetMonthHrs = case when isnull(FUT.TargetMonthHrs,0) = 0 then 1 else FUT.TargetMonthHrs  end
		FROM FMS_UNITS FU with (nolock) 
		LEFT OUTER JOIN FMS_UNIT_TYPES FUT with (nolock) ON FUT.UnitTypeID = FU.UnitTypeID 
		LEFT OUTER JOIN FMS_COMPANIES FCO with (nolock) ON FCO.CompanyCode = FU.CompanyCode 
		LEFT OUTER JOIN FMS_SCHEDULE_TYPES FST with (nolock) ON FST.UnitSchedCode=FU.UnitSchedType
		INNER JOIN
		(
			SELECT FIM.InvDate,Fim.InvoiceCode, fid.UnitCode,fid.ActAmount,fid.AllocHrs,fid.AllocExpAmt,fid.ActQty
			from FMS_INVOICE_MASTER fim JOIN FMS_INVOICE_DETAILS fid on fim.InvoiceCode = fid.InvoiceCode
			
		)Inv ON  FU.UnitCode = Inv.UnitCode
		where FU.UnitTypeID IN (SELECT UnitTypeID FROM FMS_UNIT_TYPES WHERE TrackUnit = 1) 
		--and (FU.CompanyCode = @CompanyCode  OR isnull(FU.CompanyCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_AllCompany' in (@CompanyCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'C')))
		--and (FU.YardCode in (@YardCode) OR isnull(FU.YardCode,'') in (SELECT Company from DBO.FMS_FN_GET_COMPANY_YARD_DEPT (CASE WHEN '_AllYards' in (@YardCode) then dbo.FMS_FN_LOGGED_USER() else '' end,'Y')))
		--and (FU.CompanyCode = @CompanyCode  OR @CompanyCode='_All')
		--and (FU.YardCode in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@YardCode,',')) OR @YardCode='_All')
		and FU.Status = 1 
		and (FST.Description like '%Crane%' or FST.Description like '%Lift%')
		and month(Inv.InvDate) = @Month and  Year(Inv.InvDate) = @Year
		--and (FU.UnitTypeID in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitTypeID,',')) OR ('-1' in (@UnitTypeID)))
		--and (FU.UnitSchedType in (select Value from [dbo].[FMS_FN_STRINGSPLIT](@UnitSchedType,',')) OR ('_All' in (@UnitSchedType)))
		order by Inv.UnitCode,InvoiceCode