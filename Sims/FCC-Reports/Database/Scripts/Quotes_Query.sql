select  top 100 qm.QuoteNumb as QuoteNo,
		qm.CompanyCode,
		c.Name as CompanyName,
		qm.YardCode,
		Y.YardName,
		qm.DeptCode,
		d.Name as DeptName,
		qm.CustCode,
		c1.Name1 + c1.Name2 as CustomerName,
		qm.QuotedTo,
		qm.JobCode,
		qm.JobSDate as JobStartDate,
		qm.JobEDate as JobEndDate,
		qm.FinalDate as FinalDate,
		qm.SubmitDate as SubmitDate,
		qm.AwardDate as AwardDate,
		qm.QuoteMin as QuoteMinAmount,
		qm.QuoteMax as QuoteMaxAmount,
		qm.SpersonCode as SalesPersonCode,
		sp.LName + ',' + sp.FName as SalesPerson,
		qs.Description as QuoteStatus,
		lq.Reason as LostQuoteReason,
		--qm.WorkDesc,
		qm.CreatedBy,
		qm.CreatedDate,
		qm.ModifiedBy,
		qm.ModifiedDate
FROM FMS_QUOTE_MASTER qm
left outer join FMS_Companies c on qm.CompanyCode = c.Companycode
left outer join FMS_YARD_MASTER y on qm.YardCode = y.YardCode
left outer join FMS_DEPARTMENTS d on qm.DeptCode = d.DeptCode
left outer join FMS_CUSTOMERS c1 on qm.CustCode = c1.CustCode
left outer join FMS_SALES_PERSON sp on qm.SpersonCode = sp.SpersonCode
left outer join FMS_QUOTE_STATUS qs on qm.QuoteStatusCode = qs.QuoteStatusCode
left outer join FMS_LOST_QUOTES lq on qm.LostQuoteCode = lq.LostQuoteCode
order by qm.CreatedDate desc
for json path, Root('Quotes')
